from django.db import models

# Create your models here.
class CustomerLogin(models.Model):
    id = models.AutoField(primary_key=True, null=False)
    username = models.CharField(max_length=100, default=0)
    email = models.EmailField(max_length=100, default=0)
    password = models.CharField(max_length=16, default=0)
    confirm_password = models.CharField(max_length=16, default=0)
    token = models.CharField(max_length=200, default=None, null=True)
    tokenCreatedAt = models.DateTimeField(default=None, null=True)


class CustomerBooking(models.Model):
    id = models.AutoField(primary_key=True, null=False)
    User_id = models.ForeignKey(CustomerLogin, db_column='user_id', on_delete=models.CASCADE)
    Name = models.CharField(max_length=100, default=0)
    Mobile_Number = models.CharField(max_length=10, default=0)
    PickUp_Address = models.CharField(max_length=300, default=0)
    Pin_Code= models.IntegerField()
    Destination_Address = models.CharField(max_length=300, default=0)
    PickUp_Date = models.DateField()
    PickUp_Time = models.TimeField()

class DriverLogin(models.Model):
    id = models.AutoField(primary_key=True, null=False)
    username = models.CharField(max_length=100,  default=0)
    email= models.EmailField(max_length=100, default=0)
    password = models.CharField(max_length=16, default=0)
    confirm_password = models.CharField(max_length=16, default=0)
    token = models.CharField(max_length=200, default=None, null=True)
    tokenCreatedAt = models.DateTimeField(default=None, null=True)

