from django.apps import AppConfig


class IntercityConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'interCity'
