from django.core.mail import send_mail
from django.http import HttpResponse

'''
def sendSimpleEmail():
    res = send_mail("hello paul", "This is a test email", "gauravkolambe1928@gmail.com",
                    ["gauravkolambe19@gmail.com", "gauravpatil32@gmail.com"])

    return HttpResponse('{}'.format(res))
'''

def sendEmail(recepients, subject, email_body):
    res = send_mail(subject=subject, message=None, from_email="gauravkolambe1928@gmail.com", recipient_list=recepients,
                    fail_silently=True, html_message=email_body)
    return HttpResponse('{}'.format(res))
