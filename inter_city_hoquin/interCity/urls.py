from django.urls import path
from . import views
from . import driver_signin
from django.contrib.auth import views as auth_views
urlpatterns = [
   path('',views.index ,name="index"),
   path('about',views.about,name="about"),
   path('pricing',views.pricing,name="pricing"),
   path('car',views.car,name="car"),
   path('Book_now',views.Book_now,name="Book_now"),
   path('car_single',views.car_single,name="car_single"),
   path('blog',views.blog,name="blog"),
   path('contact',views.contact,name="contact"),
   path('login/<usertype>/',views.login,name="login"),
   path('sign_in_check/<usertype>/',views.sign_in_check,name="sign_in_check"),
   path('sign_in//<usertype>/',views.sign_in,name ="sign_in"),
   path('sign_up/<usertype>/',views.sign_up,name="sign_up"),
   path('logout',views.logout,name="logout"),
   path("password_reset/<usertype>/", views.password_reset, name="password_reset"),
   path("reset/password/done/", views.request_password_reset, name="request_password_reset"),
   path("password_reset-confirm/<usertype>/<uid>/", views.password_reset_confirm, name="password_reset_confirm"),
   path("password_reset_complete", views.password_reset_complete, name="password_reset_complete"),
   # path('sendmail', views.sendmail, name="sendmail")


]
