# Generated by Django 3.2.4 on 2021-06-14 10:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('interCity', '0003_remove_userrole_userid'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userrole',
            old_name='password',
            new_name='password1',
        ),
        migrations.RenameField(
            model_name='userrole',
            old_name='Confirm_password',
            new_name='password2',
        ),
    ]
