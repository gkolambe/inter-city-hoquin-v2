from django.shortcuts import render,redirect,HttpResponse
from .models import CustomerLogin, CustomerBooking, DriverLogin
from datetime import datetime, timedelta
from .utils.email import sendEmail
import uuid


# Create your views here.
def index(request):
    return render(request, "index.html")


def about(request):
    return render(request,"about.html")


def pricing(request):
    return render(request,"pricing.html")


def car(request):
    return render(request,"car.html")


def Book_now(request):
    if request.method == 'POST':
        # C= CustomerLogin.objects.get(id =1)
        C = CustomerLogin.objects.get(id=request.session['user_id'])
        # User_id=request.session['user_id']
        User_id=C
        Name = request.POST['Name']
        Mobile_Number = request.POST['Mobile_Number']
        PickUp_Address = request.POST['PickUp_Address']
        Pin_Code= request.POST['Pin_code']
        Destination_Address= request.POST['Destination_Address']
        print("request.POST['PickUp_Date'] :: {}".format(request.POST['PickUp_Date']))
        PickUp_Date= datetime.strptime(request.POST['PickUp_Date'], "%d/%m/%Y").strftime("%Y-%m-%d")
        PickUp_Time= request.POST['PickUp_Time']

        app = CustomerBooking(User_id=User_id,Name=Name, Mobile_Number=Mobile_Number, PickUp_Address=PickUp_Address, Pin_Code=Pin_Code, Destination_Address=Destination_Address, PickUp_Date=PickUp_Date, PickUp_Time=PickUp_Time)
        app.save()
        return HttpResponse('Sucessfully Book')
    else:
        return render(request, "booking.html")


def car_single(request):
    return render(request, "car_single.html")


def blog(request):
    return render(request, "blog.html")


def contact(request):
    return render(request, "contact.html")


# LOGIN  Module
def login(request, usertype):

    return render(request, "login.html", {
        'usertype': usertype
    })


def sign_in_check(request,usertype):
    #usertype = request.POST['usertype']
    if request.method == 'POST':
        context = {
            'error_message': None
            }

        if usertype == 'customer':
            user_query = CustomerLogin.objects.filter(email=request.POST['email'], password=request.POST['password'])

            print("user_query.query: {}".format(user_query.query))
            user = user_query.first()
            #user.save()
            context['error_message'] = 'Customer Not Found Please SignUP'

        else:
            user_query = DriverLogin.objects.filter(email=request.POST['email'], password=request.POST['password'])
            user = user_query.first()
           # user.save()
            context['error_message'] = 'Driver Not Found Please SignUP'

        if user is not None:
            request.session['username'] = user.username
            request.session['user_id'] = user.id
            return redirect('/sign_in/{}'.format(usertype))
        else:

            return render(request, 'login.html'.format(usertype), context)



def sign_in(request,usertype):
    if request.method == 'POST':
        context = {
            "usertype": usertype
        }

        if 'username' in request.session:
            if usertype == 'customer':
                current_user = request.session['username']
                param = {'current_user': current_user}

                return redirect(request,"sign_in.html",param )
            else:
                current_user =  request.session['username']
                param = {'current_user':current_user}

                return redirect(request,"driver_register.html",param,context)
        else:
            return render(request,"login.html",)


def password_reset (request, usertype):
    return render(request, 'password_reset.html', {
        'usertype': usertype
    })


def sign_up(request,usertype):
    if request.method == 'POST':
        context = {
            "usertype":usertype
        }
        if usertype == "customer":
            user = CustomerLogin(username=request.POST['username'], email=request.POST['email'], password=request.POST['password'],confirm_password=request.POST['confirm_password'])
            user.save()

        else:
            user = DriverLogin(username=request.POST['username'], email=request.POST['email'], password=request.POST['password'],confirm_password=request.POST['confirm_password'])
            user.save()
        return redirect('/login/{}'.format(usertype),context)
    else:
        return redirect('/sign_up/'.format(usertype))




def logout(request):

    session_keys = [*request.session.keys()]
    for key in session_keys:
        del request.session[key]
    # del request.session['username']
    return render(request, "index.html")


def request_password_reset(request):
    email_address = request.POST['email']
    usertype = request.POST['usertype']
    uid = uuid.uuid4()
    if request.method == 'POST':
        if usertype == "customer":
            user = CustomerLogin.objects.filter(email=email_address).first()
        else:
            user = DriverLogin.objects.filter(email=email_address).first()
        user.token = uid
        user.tokenCreatedAt = datetime.now()
        user.save()
        password_reset_link = 'http://localhost:8000/password_reset-confirm/{}/{}'.format(usertype, uid)
        sendEmail([email_address], "Request for password reset",
              "Click the below link to reset your password: <br> <a href='{}' target='_blank'>{}</a>".format(
                  password_reset_link, password_reset_link))
        return render(request, "password_reset_done.html")


def password_reset_confirm(request, usertype, uid):
    if request.method == 'GET':
        context = {
            "uid": None,
            "error_message": None,
            "usertype": usertype
        }
        if usertype == 'customer':
            user = CustomerLogin.objects.filter(token=uid).first()
        else:
            user = DriverLogin.objects.filter(token=uid).first()

        # customerlogin = CustomerLogin.objects.filter(token=uid).first()
        if user is not None:
            token_expires_at = user.tokenCreatedAt.replace(tzinfo=None) + timedelta(minutes=15)
            if token_expires_at > datetime.now():
                print("Request: {}".format(uid))
                # request.session['uid'] = uid
                context['uid'] = uid
                return render(request, "password_reset_confirm.html", context)
            else:
                print("Request: {}".format(uid))
                user.token = None
                user.tokenCreatedAt = None
                user.save()
                context['error_message'] = 'The token has expired!'
                return render(request, "error.html", context)
        else:
            context['error_message'] = 'Invalid Token!'
            return render(request, "error.html", context)


def password_reset_complete(request):
    # Get the token from request
    token = request.POST["token"]
    usertype = request.POST["usertype"]
    context = {
        'error_message': None
    }
    # Find customer record from this token.
    if usertype == "customer":
        user = CustomerLogin.objects.filter(token=token).first()
    else:
        user = DriverLogin.objects.filter(token=token).first()
    if user is not None:
        new_password = request.POST['password']
        c_password = request.POST['confirm_password']
        if new_password == c_password:
            user.password = new_password
            user.confirm_password = c_password
            print(c_password)
            user.token = None
            user.tokenCreatedAt = None

            user.save()
            return render(request, "password_reset_complete.html")
        else:
            context['error_message'] = 'Password and confirm password does not match'
            return render(request, "error.html", context)
    else:
        context['error_message'] = 'Customer not found!'
        return render(request, "error.html", context)

'''
def sendmail(request):
    sendSimpleEmail()
    return render(request, "sample.html")
'''