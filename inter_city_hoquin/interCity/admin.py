from django.contrib import admin
# Register your models here.
from .models import CustomerLogin,CustomerBooking,DriverLogin

admin.site.register(CustomerLogin),
admin.site.register(CustomerBooking),
admin.site.register(DriverLogin)